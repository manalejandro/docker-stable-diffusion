# Docker Stable Diffusion

## Dirty docker project to run Stable Diffusion AI with [this](https://github.com/basujindal/stable-diffusion/) optimized project for low memory Nvidia graphics cards ~ 4GB, final Docker image must be 22.3GB of size

### This project need `docker` with `docker-compose` optional, necessary [nvidia runtime](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/user-guide.html) and [latest nvidia drivers](https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html#ubuntu-lts) installed

## Build

```
docker-compose build
```

## Run

```
docker-compose run --rm stable-diffusion "input terms"
```

## Output image

```
JPG file output is in the ./output folder
```

## Generate from existing image

```
docker-compose run --rm --entrypoint "python optimizedSD/optimized_img2img.py" stable-diffusion --outdir /output --n_samples 1 --precision full --prompt "input terms" --format jpg --H 512 --W 512 --init-img /output/input.png
```

## License

```
MIT
```
